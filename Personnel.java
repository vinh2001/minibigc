/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniproject;

/**
 *
 * @author lemin
 */
public class Personnel {
    String id,fullname,gender,address,email,numberphone,CMND,position,account,password;
    int age;

    public Personnel() {
    }

    public Personnel( String id, String fullname, int age, String gender, String address, String email, String numberphone, String CMND, String position, String account, String password) {
        this.id = id;
        this.fullname = fullname;
        this.gender = gender;
        this.address = address;
        this.email = email;
        this.numberphone = numberphone;
        this.CMND = CMND;
        this.position = position;
        this.account = account;
        this.password = password;
        this.age = age;
    }

    public Personnel(String id, String fullname, int age, String gender, String address, String email, String numberphone, String CMND, String position) {
        this.id = id;
        this.fullname = fullname;
        this.gender = gender;
        this.address = address;
        this.email = email;
        this.numberphone = numberphone;
        this.CMND = CMND;
        this.position = position;
        this.age = age;
    }

    


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumberphone() {
        return numberphone;
    }

    public void setNumberphone(String numberphone) {
        this.numberphone = numberphone;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    
}
