/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniproject;

/**
 *
 * @author lemin
 */
public class OrderDetail {
    int id;
    String name;
    int amount, price;
    String buydate;
    int sumprice;
    String employeeid;

    public OrderDetail() {
    }

    public OrderDetail(int id, String name, int amount, int price, String buydate, int sumprice, String employeeid) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.buydate = buydate;
        this.sumprice = sumprice;
        this.employeeid = employeeid;
    }

    public OrderDetail(String name, int amount, int price, String buydate, String employeeid) {
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.buydate = buydate;
     
        this.employeeid = employeeid;
    }

    public OrderDetail(String name, int amount, int price, String buydate, int sumprice, String employeeid) {
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.buydate = buydate;
        this.sumprice = sumprice;
        this.employeeid = employeeid;
    }

    public OrderDetail(int id, String name, int amount, int price, String buydate, String employeeid) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.buydate = buydate;
        this.employeeid = employeeid;
    }

   
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getBuydate() {
        return buydate;
    }

    public void setBuydate(String buydate) {
        this.buydate = buydate;
    }

    public int getSumprice() {
        sumprice = amount * price;
        return sumprice;
    }

  

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }
    
    
}
