/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lemin
 */
public class ProductModify {
    public static List<Product> findAll(){
        
        List<Product> productList = new ArrayList<>();
                
        Connection connection = null;
        
        Statement statement = null;
        try {
            //lay tat ca danh sach nhan vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from product";
            statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery(sql);
            
            while (resultSet.next()) {
               Product pr = new Product(
                       resultSet.getString("id"),
                       resultSet.getString("name"),
                       resultSet.getString("supplier"),
                       resultSet.getString("provider"),
                       resultSet.getString("adddate"),
                       resultSet.getInt("amount"),
                       resultSet.getFloat("weight"),
                       resultSet.getInt("price"),
                       resultSet.getString("category"));
               productList.add(pr);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //ket thuc
        
        return productList;
    }
    public static Product findByID(String id){
         Connection connection = null;
        
        PreparedStatement statement = null;
         try {
            
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from product where id = ?";
            
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                Product pr = new Product(
                       resultSet.getString("id"),
                       resultSet.getString("name"),
                       resultSet.getString("supplier"),
                       resultSet.getString("provider"),
                       resultSet.getString("adddate"),
                       resultSet.getInt("amount"),
                       resultSet.getFloat("weight"),
                       resultSet.getInt("price"),
                       resultSet.getString("category"));
                return pr;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return null;
       
    }
    
    public static List<Product> findByName(String name){
         Connection connection = null;
        
        PreparedStatement statement = null;
        List<Product> List = new ArrayList<>();
         try {
            
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from product where name like ?";
            
            statement = connection.prepareStatement(sql);
            statement.setString(1, "%"+name+"%");
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                Product pr = new Product(
                       resultSet.getString("id"),
                       resultSet.getString("name"),
                       resultSet.getString("supplier"),
                       resultSet.getString("provider"),
                       resultSet.getString("adddate"),
                       resultSet.getInt("amount"),
                       resultSet.getFloat("weight"),
                       resultSet.getInt("price"),
                       resultSet.getString("category"));
                List.add(pr);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return List;
       
    }
    
    
    public static void insert(Product pr) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            //lay tat ca danh sach sinh vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            
            //query
            String sql = "insert into product(id, name, supplier, provider, adddate , amount, weight, price, category) values(?, ?, ?, ?, Convert(?,datetime), ?, ?, ?, ?)";
            statement = connection.prepareCall(sql);
            
            statement.setString(1, pr.getId());
            statement.setString(2, pr.getName());
            statement.setString(3, pr.getSupplier());
            statement.setString(4, pr.getProvider());
            statement.setString(5, pr.getAdddate());
            statement.setInt(6, pr.getAmount());
            statement.setFloat(7, pr.getWeight());
            statement.setInt(8, pr.getPrice());
            statement.setString(9, pr.getCategory());
            
            statement.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //ket thuc.
    }
    
    public static void update(Product pr) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            //lay tat ca danh sach sinh vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            
            //query
            String sql = "update product set name=?,supplier=?,provider=?,adddate=Convert(?,datetime),amount=?,weight=?,price=?,category=? where  id=? ";
            statement = connection.prepareCall(sql);
            
            statement.setString(1, pr.getName());
            statement.setString(2, pr.getSupplier());
            statement.setString(3, pr.getProvider());
            statement.setString(4, pr.getAdddate());
            statement.setInt(5, pr.getAmount());
            statement.setFloat(6, pr.getWeight());
            statement.setInt(7, pr.getPrice());
            statement.setString(8, pr.getCategory());
            statement.setString(9, pr.getId());
            
            statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProductModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //ket thuc.
    }
}
