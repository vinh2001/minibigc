/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lemin
 */
public class PersonnelModify {
    public static List<Personnel> findAll(){
        
        List<Personnel> personnelList = new ArrayList<>();
                
        Connection connection = null;
        
        Statement statement = null;
        try {
            //lay tat ca danh sach nhan vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from personnel";
            statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery(sql);
            
            while (resultSet.next()) {
                Personnel psn = new Personnel(
                            resultSet.getString("id"),
                            resultSet.getString("fullname"),
                            resultSet.getInt("age"),
                            resultSet.getString("gender"),
                            resultSet.getString("address"),
                            resultSet.getString("email"),
                            resultSet.getString("numberphone"),
                            resultSet.getString("CMND"),
                            resultSet.getString("position"),
                            resultSet.getString("account"),
                            resultSet.getString("password"));
                personnelList.add(psn);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //ket thuc
        
        return personnelList;
    }
    public static Personnel findByID(String id){
         Connection connection = null;
        
        PreparedStatement statement = null;
         try {
            
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from personnel where id = ?";
            
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                Personnel psn = new Personnel(
                            resultSet.getString("id"),
                            resultSet.getString("fullname"),
                            resultSet.getInt("age"),
                            resultSet.getString("gender"),
                            resultSet.getString("address"),
                            resultSet.getString("email"),
                            resultSet.getString("numberphone"),
                            resultSet.getString("CMND"),
                            resultSet.getString("position"),
                            resultSet.getString("account"),
                            resultSet.getString("password"));
                return psn;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return null;
       
    }
    
    public static void insert(Personnel psn) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            //lay tat ca danh sach sinh vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            
            //query
            String sql = "insert into personnel(id, fullname, age, gender, address , email, numberphone, CMND, position, account, password) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            statement = connection.prepareCall(sql);
            
            statement.setString(1, psn.getId());
            statement.setString(2, psn.getFullname());
            statement.setInt(3, psn.getAge());
            statement.setString(4, psn.getGender());
            statement.setString(5, psn.getAddress());
            statement.setString(6, psn.getEmail());
            statement.setString(7, psn.getNumberphone());
            statement.setString(8, psn.getCMND());
            statement.setString(9, psn.getPosition());
            statement.setString(10, psn.getAccount());
            statement.setString(11, psn.getPassword());
            
            statement.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //ket thuc.
    }
    
    public static void update(Personnel psn) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            //lay tat ca danh sach sinh vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            
            //query
            String sql = "update personnel set fullname=?,age=?,gender=?,address=?,email=?,numberphone=?,CMND=?,position=? where  id=? ";
            statement = connection.prepareCall(sql);
            
            statement.setString(1, psn.getFullname());
            statement.setInt(2, psn.getAge());
            statement.setString(3, psn.getGender());
            statement.setString(4, psn.getAddress());
            statement.setString(5, psn.getEmail());
            statement.setString(6, psn.getNumberphone());
            statement.setString(7, psn.getCMND());
            statement.setString(8, psn.getPosition());
            statement.setString(9, psn.getId());
            
            statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PersonnelModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //ket thuc.
    }
}
