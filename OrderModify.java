/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miniproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lemin
 */
public class OrderModify {
    public static List<OrderDetail> findAll(){
        
        List<OrderDetail> orderdetailList = new ArrayList<>();
                
        Connection connection = null;
        
        Statement statement = null;
        try {
            //lay tat ca danh sach nhan vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from orderdetail";
            statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery(sql);
            
            while (resultSet.next()) {
               OrderDetail od = new OrderDetail(
                       resultSet.getInt("id"),
                       resultSet.getString("name"),
                       resultSet.getInt("amount"),
                       resultSet.getInt("price"),
                       resultSet.getString("buydate"),
                       resultSet.getInt("sumprice"),
                       resultSet.getString("employeeid"));
               orderdetailList.add(od);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //ket thuc
        
        return orderdetailList;
    }
    public static OrderDetail findByID(int id){
         Connection connection = null;
        
        PreparedStatement statement = null;
         try {
            
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            //query
            String sql = "select * from orderdetail where id = ?";
            
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            
            ResultSet resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                OrderDetail od = new OrderDetail(
                       resultSet.getString("name"),
                       resultSet.getInt("amount"),
                       resultSet.getInt("price"),
                       resultSet.getString("buydate"),
                       resultSet.getInt("sumprice"),
                       resultSet.getString("employeeid"));
                return od;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        return null;
       
    }
    
    
    
    public static void insert(OrderDetail od) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            //lay tat ca danh sach sinh vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            
            //query
            String sql = "insert into orderdetail( name, amount, price, buydate , sumprice, employeeid) values(?, ?, ?, Convert(?,datetime), ?, ?)";
            statement = connection.prepareCall(sql);
            
            statement.setString(1, od.getName());
            statement.setInt(2, od.getAmount());
            statement.setInt(3, od.getPrice());
            statement.setString(4, od.getBuydate());
            statement.setInt(5, od.getSumprice());
            statement.setString(6, od.getEmployeeid());
            
            statement.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //ket thuc.
    }
    
    public static void update(OrderDetail od) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            //lay tat ca danh sach sinh vien
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", "root", "");
            
            //query
            String sql = "update orderdetail set name=?,amount=?,price=?,buydate=Convert(?,datetime),sumprice=?,employeeid=? where  id=? ";
            statement = connection.prepareCall(sql);
            
            statement.setString(1, od.getName());
            statement.setInt(2, od.getAmount());
            statement.setInt(3, od.getPrice());
            statement.setString(4, od.getBuydate());
            statement.setInt(5, od.getSumprice());
            statement.setString(6, od.getEmployeeid());
            statement.setInt(7, od.getId());
            
            statement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(OrderModify.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //ket thuc.
    }
}
